provider "google" {
  credentials = "${file("./creds/erol-sandbox-39f4c04f5b6c.json")}"
  project     = "gke-tf-demo"
  region      = "europe-west1"
}
